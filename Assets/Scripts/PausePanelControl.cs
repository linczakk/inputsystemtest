using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class PausePanelControl : MonoBehaviour
{
    private InputAction escapeMenu;
    private bool escapeMenuOpen = false;
    [SerializeField]
    private GameObject pauseMenuPanel;


    private void OnEnable()
    {
        escapeMenu = InputManager.inputActions.Player.EscMenu;
        escapeMenu.performed += OnEscapeButton;
        escapeMenu.Enable();

    }

    private void OnEscapeButton(InputAction.CallbackContext obj)
    {
        escapeMenuOpen = !escapeMenuOpen;
        pauseMenuPanel.SetActive(escapeMenuOpen);

        if (escapeMenuOpen)
            InputManager.ToggleActionMap(InputManager.inputActions.UI);
        else
            InputManager.ToggleActionMap(InputManager.inputActions.Player);
    }

    private void OnDisable()
    {
        escapeMenu.Disable();
    }
}
