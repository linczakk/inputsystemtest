using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Attacks : MonoBehaviour
{
    [SerializeField]
    private float OffsetShootingPoint;
    
    [SerializeField]
    private GameObject laserPrefab;
    
    [SerializeField]
    private GameObject lasersParent;



    //private void OnEnable()
    //{
    //    inputActions.Player.Fire.performed += ShootLaser => {
    //        var missile = Instantiate(laserPrefab, new Vector3(transform.position.x, transform.localPosition.y + OffsetShootingPoint, transform.position.z), 
    //                    transform.rotation, lasersParent.transform);
    //        Debug.Log(transform.localRotation);

    //    };
    //    inputActions.Player.Fire.Enable();
    //}

    #region Inputs
    public void OnFire(InputAction.CallbackContext value)
    {
        var fireButtonPressed = value.started;

        if(fireButtonPressed)
        {
            var missile = Instantiate(laserPrefab, new Vector3(transform.position.x, transform.localPosition.y + OffsetShootingPoint, transform.position.z),
                        transform.rotation, lasersParent.transform);
            Debug.Log(transform.localRotation);
        }
    }
    #endregion

    //private void OnDisable()
    //{
    //    inputActions.Player.Fire.Disable();
    //}

    //private void ShootLaser InputAction.CallbackContext obj)
    //{
    //    Debug.Log("Jump :D");
    //}

}
