﻿using System.Collections;
using UnityEngine;

public class Missile : MonoBehaviour
{
    [SerializeField]
    protected float DeactivateDelay = 5;
    protected int Damage = 10;

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        var enemy = collision.GetComponent<IDamagable>();
        enemy?.TakeDamage(Damage);

    }

    protected IEnumerator DeactivateAfterDelay(float delay)
    {
        var wait = new WaitForSeconds(delay);
        yield return wait;
        Deactivate();

    }

    protected void Deactivate()
    {
        Destroy(this.gameObject);
    }
}




