﻿public interface IDamagable
{
    public int HealthAmount { get; set; }
    void TakeDamage(int damage);
    void DestroyObject();
}

