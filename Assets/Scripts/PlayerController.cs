using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

public class PlayerController : MonoBehaviour
{
    private TestingInputActions inputActions;
    private InputAction movement;
    private Vector2 moveDirection;
    private Rigidbody2D rb;
    [SerializeField]
    private Camera cam;

    [SerializeField]
    private float speed;
    [SerializeField]
    private float maxVelocity = 3;
    [SerializeField]
    private float rotationSpeed = 3;


    private bool held = false;
    private float screenHalfWidthInWorldUnits;

    private bool openEscMenu = false;


    private void Awake()
    {
        inputActions = new TestingInputActions();
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        var halfPlayerWidth = transform.localScale.x / 2;
        screenHalfWidthInWorldUnits = cam.aspect * cam.orthographicSize + halfPlayerWidth;
    }

    private void OnEnable()
    {
        movement = inputActions.Player.Movement;
        movement.Enable();

        inputActions.Player.Brake.performed += Brakes => held = true;
        inputActions.Player.Brake.canceled += Brakes => held = false;
        inputActions.Player.Brake.Enable();

        //inputActions.Player.EscMenu.performed += PauseMenu;
        //inputActions.Player.EscMenu.Enable();

        //inputActions.Player.Fire.Enable();
    }

    private void PauseMenu(InputAction.CallbackContext obj)
    {
        openEscMenu = !openEscMenu;
        UIControll.Instance.SetPausePanel(openEscMenu);
    }

    private void OnDisable()
    {
        movement.Disable();
        inputActions.Player.Brake.Disable();
        //inputActions.Player.EscMenu.Disable();
    }


    private void Update()
    {
        ProcessInput();

        if(transform.position.x < -screenHalfWidthInWorldUnits)
        {
            transform.position = new Vector2(screenHalfWidthInWorldUnits, transform.position.y);
        }
        if(transform.position.x > screenHalfWidthInWorldUnits)
        {
            transform.position = new Vector2(-screenHalfWidthInWorldUnits, transform.position.y);
        }

    }

    private void ProcessInput()
    {
        moveDirection = new Vector2(this.movement.ReadValue<Vector2>().x, this.movement.ReadValue<Vector2>().y);
    }

    //public void OnMovementInput(InputAction.CallbackContext value)
    //{
    //    moveDirection = new Vector2(this.movement.ReadValue<Vector2>().x, this.movement.ReadValue<Vector2>().y);
    //}

    private void FixedUpdate()
    {
        ThrustForward(moveDirection.y);
        Rotate(rb, -moveDirection.x * rotationSpeed);
        Debug.Log("Movement Values " + this.movement.ReadValue<Vector2>());
    }

   
    private void ThrustForward(float amount)
    {
        var force = transform.up * amount;

        if(rb.velocity.magnitude < maxVelocity)
            rb.AddForce(force);
    }

    private void Rotate(Rigidbody2D rb2D, float amount)
    {
        rb2D.MoveRotation(rb2D.rotation + amount * Time.fixedDeltaTime);
    }

    public void OnMenuToogle(InputAction.CallbackContext value)
    {
        var pausePressed = value.started;
        if(pausePressed)
        {
            UIControll.Instance.Pause(!UIControll.Instance.GetPausePanel().activeSelf);
        }
    }

    private void Brakes()
    {

        rb.velocity = Vector2.Lerp(rb.velocity, Vector2.zero, 0.2f);
        Debug.Log("Velocity" + rb.velocity);
    }



    //private void Brakes(InputAction.CallbackContext obj)
    //{
    //    rb.velocity = Vector2.Lerp(rb.velocity, Vector2.zero, 0.2f);
    //    Debug.Log("Velocity" + rb.velocity);
    //}
}
