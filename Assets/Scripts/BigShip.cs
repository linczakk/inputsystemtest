﻿using System.Collections;
using UnityEngine;

namespace Units
{
    public class BigShip : MonoBehaviour, IDamagable
    {
        public int HealthAmount { get; set; }

        private void Awake()
        {
            HealthAmount = 100;
        }

        public void TakeDamage(int damage)
        {
            HealthAmount -= damage;
            Debug.Log("Ała, zadałeś mi: " + damage +" obrażeń!");
            Debug.Log("Pozostało mi: " + HealthAmount + " punktów życia");

            OnGettingHit();

            if (HealthAmount <= 0)
                DestroyObject();
        }

        private void OnGettingHit()
        {
            var spriteRenderer = transform.GetComponentInChildren<SpriteRenderer>();
            spriteRenderer.color = Color.red;
            StopAllCoroutines();
            StartCoroutine(TurnWhite(spriteRenderer));

        }

        private IEnumerator TurnWhite(SpriteRenderer spriteRenderer)
        {
            yield return new WaitForSeconds(0.2f);
            spriteRenderer.color = Color.white;
        }


        public void DestroyObject()
        {
            Destroy(this.gameObject);
        }
    }
}
