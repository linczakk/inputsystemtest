using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Missile
{
    public float speed = 20f;
    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        Damage = 20;
    }
    private void Start()
    {
        StartCoroutine(DeactivateAfterDelay(DeactivateDelay));
        rb.velocity = transform.up * speed;
    }

}

