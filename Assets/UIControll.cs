using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIControll : MonoBehaviour
{
    private static UIControll _instance;

    public static UIControll Instance { get { return _instance; } }
    public static bool isPaused;

    private void Awake()
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }


    [SerializeField]
    private GameObject pausePanel;

    public GameObject GetPausePanel() => pausePanel;
    
    public void Pause(bool on)
    {
        if(on)
        {
            Time.timeScale = 0;
            InputManager.ToggleActionMap(InputManager.inputActions.UI);
            isPaused = true;
            SetPausePanel(true);
        }
        else
        {
            Time.timeScale = 1;
            InputManager.ToggleActionMap(InputManager.inputActions.Player);
            isPaused = false;
            SetPausePanel(false);
        }
    }

    public void SetPausePanel(bool shouldBeActive)
    {
        pausePanel.SetActive(shouldBeActive);
    }


}
